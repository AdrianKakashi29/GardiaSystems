﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nt45 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.BlockedIp");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BlockedIp",
                c => new
                    {
                        IPAddress = c.String(nullable: false, maxLength: 128),
                        Reason = c.String(),
                    })
                .PrimaryKey(t => t.IPAddress);
            
        }
    }
}
