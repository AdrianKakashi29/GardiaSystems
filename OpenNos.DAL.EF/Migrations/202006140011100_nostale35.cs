﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nostale35 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "TattooCount", c => c.Byte(nullable: false));
            DropColumn("dbo.CharacterSkill", "IsTattoo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CharacterSkill", "IsTattoo", c => c.Boolean(nullable: false));
            DropColumn("dbo.Character", "TattooCount");
        }
    }
}
