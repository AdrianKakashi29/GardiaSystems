﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _202003022014009_NpcMessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MapNpc", "Say", c => c.String());
            AddColumn("dbo.MapNpc", "Delay", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MapNpc", "Delay");
            DropColumn("dbo.MapNpc", "Say");
        }
    }
}
