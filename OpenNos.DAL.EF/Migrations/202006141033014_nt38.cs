﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nt38 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CharacterSkill", "IsTattoo", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CharacterSkill", "IsTattoo");
        }
    }
}
