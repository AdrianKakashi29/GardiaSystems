﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nt42 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlockedIp",
                c => new
                    {
                        IPAddress = c.String(nullable: false, maxLength: 128),
                        Reason = c.String(),
                    })
                .PrimaryKey(t => t.IPAddress);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BlockedIp");
        }
    }
}
