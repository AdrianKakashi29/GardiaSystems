﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nt40 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemInstance", "RuneUpgrade", c => c.Byte(nullable: false));
            AddColumn("dbo.ItemInstance", "RuneBroke", c => c.Boolean(nullable: false));
            AddColumn("dbo.ShellEffect", "IsRune", c => c.Boolean(nullable: false));
            AddColumn("dbo.ShellEffect", "RuneUpgrade", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ShellEffect", "RuneUpgrade");
            DropColumn("dbo.ShellEffect", "IsRune");
            DropColumn("dbo.ItemInstance", "RuneBroke");
            DropColumn("dbo.ItemInstance", "RuneUpgrade");
        }
    }
}
