﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nostale36 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CharacterSkill", "TattooCount");
        }
        
        public override void Down()
        {
            
        }
    }
}
