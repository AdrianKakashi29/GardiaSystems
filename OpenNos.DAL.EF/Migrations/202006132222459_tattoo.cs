﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tattoo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CharacterSkill", "TattooUpgrade", c => c.Int(nullable: false));
            AddColumn("dbo.CharacterSkill", "TattooCount", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CharacterSkill", "TattooCount");
            DropColumn("dbo.CharacterSkill", "TattooUpgrade");
        }
    }
}
