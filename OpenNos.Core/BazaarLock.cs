﻿using OpenNos.Core.Networking.Communication.Scs.Server;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenNos.Core
{
    public sealed class BazaarLock
    {
        static BazaarLock()
        { }

        readonly static ConcurrentDictionary<long, object> LockedItems = new ConcurrentDictionary<long, object>();

        public static ThreadSafeSortedList<long, int> AmountList = new ThreadSafeSortedList<long, int>();

        public static object GetLocker(long id)
        {
            try
            {
                var locker2 = LockedItems.GetOrAdd(id, new object());
                return locker2;
            }
            catch
            {
                return new KeyValuePair<object, int>();
            }
        }

        public static long CheckValue(long id)
        {
            try
            {
                return AmountList[id];
            }
            catch
            {
                return 0;
            }
        }

        public static void AddValue(long id, int value)
        {
            try
            {
                AmountList[id] += value;
            }
            catch
            {

            }
        }

        public bool DeleteLocker(long id)
        {
            try
            {
                LockedItems.TryRemove(id, out _);
                AmountList.Remove(id);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}